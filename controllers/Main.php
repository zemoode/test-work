<?php

class Main {

	public $db_link;
	public $is_admin = false;

	public function __construct($func, $db_link){

		$this->db_link = $db_link;
		
		include_once('models/posts.php');
		$this->posts = new Posts();

		include_once('models/users.php');
		$this->users = new Users();


		$this->$func();
	}

	public function show_view($view, $data){
		//echo $view;
		if(!is_file($view))
                return false;
       
        extract($data);
       
        ob_start();
        include($view);
        $result = ob_get_clean();

        return $result;
	}

	public function main(){
		
		$data['is_admin'] = $this->is_admin();
		
		$posts = $this->posts->get_all_posts($this->db_link);
		$data['posts'] = $posts;

		echo $this->show_view('views/header.php', $data);
		echo $this->show_view('views/tasks/containerBegin.php', $data);
		$items = "";
		foreach($posts as $item){
			//print_r($item);
			$data['item'] = $item;
			$items .= $this->show_view('views/tasks/item.php', $data);
		}

		echo $items;


		$count_tasks_items = $this->posts->get_count_posts($this->db_link);
		$data['count_tasks_items'] = (int) $count_tasks_items;
		//$data['count_tasks_items'] = $count_tasks_items;

		echo $this->show_view('views/tasks/containerEnd.php', $data);
		echo $this->show_view('views/windows.php', $data);
		echo $this->show_view('views/footer.php', $data);


		
		//eval($view);	
	}

	public function is_admin(){
		if((isset($_COOKIE['userHex']))&&($_COOKIE['userHex'])){
			if($this->users->get_user_by_hex($this->db_link, $_COOKIE['userHex'])){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function auth(){
		$data = array();
		echo $this->show_view('views/header.php', $data);
		echo $this->show_view('views/auth.php', $data);
		echo $this->show_view('views/footer.php', $data);
	} 

	public function login(){
		$login = addslashes(htmlspecialchars($_POST['login']));
		$pass = addslashes(htmlspecialchars($_POST['pass']));
		

		$is_admin = $this->users->auth($this->db_link, $login, $pass);

		if($is_admin){
			setcookie("userHex", md5($login.':'.$pass), time()+3600); 
			echo "+";
		}else{
			echo "-";
		}


	}


	public function logout(){
		setcookie("userHex", '');
		header('location: index.php'); 

	}

	public function add_post(){
		$post_text = addslashes(htmlspecialchars($_POST['post_text']));
		$post_name = addslashes(htmlspecialchars($_POST['post_name']));
		$post_email = addslashes(htmlspecialchars($_POST['post_email']));
		
		$result = $this->posts->add_post($this->db_link, $post_text, $post_name, $post_email);
		echo "+";
	}


	public function get_posts(){

		$data['is_admin'] = $this->is_admin();

		if((isset($_COOKIE['sort_name']))&&($_COOKIE['sort_name'] != "")){
			$sort_text = " ORDER BY `".$_COOKIE['sort_name']."` ASC ";	
			$data['sort_name'] = $_COOKIE['sort_name'];
		}else{
			$sort_text = " ORDER BY `id` DESC ";	
			$data['sort_name'] = 'id';
		}

		$page = addslashes(htmlspecialchars($_POST['page']));
		$posts = $this->posts->get_all_posts($this->db_link, ($page*3), 3, $sort_text);

		

		$items = "";
		foreach($posts as $item){
			//print_r($item);
			$data['item'] = $item;
			$items .= $this->show_view('views/tasks/item.php', $data);
		}

		echo $items;
	}

	public function change_post(){

		$data['is_admin'] = $this->is_admin();
		if($data['is_admin']){
			$pid = addslashes(htmlspecialchars($_POST['pid']));
			$name = addslashes(htmlspecialchars($_POST['name']));
			$value = addslashes(htmlspecialchars($_POST['value']));


			$this->posts->change_post($this->db_link, $pid, $name, $value);
			echo "+";
		}else{
			echo "-";
		}
	}

	public function change_sort(){
		$sort_name = addslashes(htmlspecialchars($_POST['sort_name']));
		setcookie("sort_name", $sort_name, time()+3600); 

	}

}