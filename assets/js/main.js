function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function send_auth_login_form(login, pass){

	var login = login;
	var pass = pass;

	$('#login_error').css('display', 'none');
	$('#pass_error').css('display', 'none');

	var validation = true;
	if(login.length <= 0){
		$('#login_error').fadeIn(500);
		validation = false;
	}

	if(pass.length <=0){
		$('#pass_error').fadeIn(500);
		validation = false;
	}

	if(validation){
		$.ajax({
	        type: 'POST',
	        data: ({'login':login, 'pass':pass}),
	        url : 'index.php?q=login',
	        async: true,
	        success: function(response){ 	        	
	         	if(response == "+"){
	         		$('#authFormError').fadeOut(500);
	         		document.location = "index.php";
	         	}else{
	         		$('#authFormError').fadeIn(500);
	         	}
	        	//
	        }
    	}); 
	}


}

function change_task_values(pid, name, value){
	//$('#TasksCOntainer').html('<div style="padding:50px; text-align:center;"><img src="assets/images/loading.gif"/></div>');
	$.ajax({
	        type: 'POST',
	        data: ({'pid':pid, 'name':name, 'value':value}),
	        url : 'index.php?q=change_post',
	        async: true,
	        success: function(response){ 	
	        	//$('#itemStatusActive'+pid).fadeIn(500);

	          //$('#TasksCOntainer').html(response);
	        }
    	}); 
}


function get_tasks(page){
	$('#TasksCOntainer').html('<div style="padding:50px; width:100%; text-align:center;"><img src="assets/images/loading.gif"/></div>');
	$.ajax({
	        type: 'POST',
	        data: ({'page':page}),
	        url : 'index.php?q=get_posts',
	        async: true,
	        success: function(response){ 	        	
	          $('#TasksCOntainer').html(response);
	          $('body').on('focus', '[contenteditable]', function() {
				    const $this = $(this);
				    $this.data('before', $this.html());
				}).on('blur keyup paste input', '[contenteditable]', function() {
				    const $this = $(this);
				    if ($this.data('before') !== $this.html()) {
				        $this.data('before', $this.html());

				        change_task_values($this.attr('pid'), $this.attr('dataname'), $this.html());
				        //alert('ichanget');
				    }
				});

	        }
    	}); 
}


function change_sort(sort_name){
	$.ajax({
	        type: 'POST',
	        data: ({'sort_name':sort_name}),
	        url : 'index.php?q=change_sort',
	        async: true,
	        success: function(response){ 	
	        	//$('#itemStatusActive'+pid).fadeIn(500);
	        	get_tasks(0);
	          //$('#TasksCOntainer').html(response);
	        }
    	}); 
}

function add_task(post_text, post_email, post_name){

	var post_text = post_text;
	var post_email = post_email;
	var post_name = post_name;

	var validation = true;


	$('#post_text_error').css('display', 'none');
	$('#post_email_error').css('display', 'none');
	$('#post_name_error').css('display', 'none');

	if(post_text.length <= 3){
		$('#post_text_error').fadeIn(500);
		validation = false;
	}

	

	if(!(isEmail(post_email))){
		$('#post_email_error').fadeIn(500);
		validation = false;
	}

	if(post_name.length <= 3){
		$('#post_name_error').fadeIn(500);
		validation = false;
	}

	if(validation){
		

		 $.ajax({
	        type: 'POST',
	        data: ({'post_text':post_text, 'post_name':post_name, 'post_email':post_email}),
	        url : 'index.php?q=add_post',
	        async: true,
	        success: function(response){ 
	        	if(response == "+"){
	        		$('#addSuccessModal').modal();
	        		get_tasks(0);		
	        	}else{
	        		$('#addErrorModal').modal();
	        	}	
	          
	          
	  
	        }
    	}); 

	}



}