<?php

class Posts {

	public function __construct(){
			
	}

	public function get_all_posts($db_link, $inpage = 0, $perpage = 3, $order_by = ""){
		$Rarray = array();
		/* Select запросы возвращают результирующий набор */
		if ($result = $db_link->query("SELECT * FROM `posts` ".$order_by." LIMIT ".$inpage.", ".$perpage)) {
			while($data = $result->fetch_assoc()){
				//print_r($data);
				$Rarray[] = $data;		
			}

		    /* очищаем результирующий набор */
		    $result->close();
		    return $Rarray;
		}

		
	}

	public function get_count_posts($db_link){
		$Rarray = array();
		if ($result = $db_link->query("SELECT * FROM `posts`")) {
			while($data = $result->fetch_assoc()){
				$Rarray[] = $data;		
			}
		    /* очищаем результирующий набор */
		    $result->close();
		    $a = count($Rarray);
		    return $a;
		}
	}


	public function add_post($db_link, $post_text, $post_name, $post_email){
		$result = $db_link->query("
			INSERT INTO `posts` 
						   (`name`, `post`,  `email`, `atDate`, `status`) 
					VALUES ('".$post_name."', '".$post_text."', '".$post_email."', '".date('Y-m-d H:i:s')."', '0')");


		return true;
	}


	public function change_post($db_link, $pid, $name, $value){
		$result = $db_link->query("
			UPDATE `posts` 
					SET `".$name."` = '".$value."' WHERE `id`= '".$pid."'");
		return true;
	}


}

?>