<div style="position:fixed; z-index:10; top:0px; left:0px; display:flex; align-items: center; justify-content: center; width:100%; height:100%; background-color:rgba(0,0,0, 0.7); ">
	<div style=" width:400px; padding:40px; position:relative; padding-top:40px;  background-color:#dddddd">
		<div onclick="$(this).parent().parent().fadeOut(500);" style="width:25px; height:25px; background-size:90%; background-image:url('assets/images/close.svg'); background-position:center; background-repeat:no-repeat; position:absolute; top:-35px; right:0px; cursor:pointer;"></div>
		<div style="font-size:24px; font-weight:700;">Авторизация</div>
		<div style="margin-top:20px;">
			<div style="margin-top:3px;">
			<div class="input-label">Логин</div>
				<div style="margin-top:3px;">
					<div style="padding:5px; display:none; padding-left:0px; font-size:12px; font-weight:bold; color:#ff5050;" id="login_error">Неодпустимое значение поля Логин.</div>
					<input type="text" id="login" class="input-text" />
				</div>
			</div>
			<div style="margin-top:3px;">
			<div class="input-label">пароль</div>
				<div style="margin-top:3px;">
					<div style="padding:5px; display:none; padding-left:0px; font-size:12px; font-weight:bold; color:#ff5050;" id="pass_error">Неправильный пароль.</div>
					<input type="password" id="pass" class="input-text" />
				</div>
			</div>

			<div id="authFormError" style="padding-top:20px; display:none; padding-bottom:20px; font-size:13px; color:#ff5050;">
				Неправильные логин или пароль попробуйте еще...
			</div>

			<div style="margin-top:20px;">
				<div onclick="send_auth_login_form($('#login').val(), $('#pass').val());" class="forms-button" style="">Войти</div>
			</div>
		</div>
	</div>
</div>