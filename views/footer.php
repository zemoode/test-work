<!-- FOOTER SCRIPS BLOCK -->


 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

 <script type="text/javascript" src="assets/js/main.js"></script>
<?php if($is_admin){ ?>
  <script>
   	$('body').on('focus', '[contenteditable]', function() {
	    const $this = $(this);
	    $this.data('before', $this.html());
	}).on('blur keyup paste input', '[contenteditable]', function() {
	    const $this = $(this);
	    if ($this.data('before') !== $this.html()) {
	        $this.data('before', $this.html());

	        change_task_values($this.attr('pid'), $this.attr('dataname'), $this.html());
	        //alert('ichanget');
	    }
	});
   </script>
<?php } ?>
  <!-- [END] FOOTER SCRIPS BLOCK -->
 </body>
 </html> 