<!DOCTYPE html>  
<html lang="en-GB">
  <head>
    <title>TEST-WORK</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    
    <!--link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">  
    <link  type="text/css" href="assets/css/main.css" rel="stylesheet" />
  

 </head>
 <body>
<div style="padding:20px; z-index:10; position:fixed; top:0px; left:0px; width:100%; padding-bottom:10px; padding-top:10px; background-color:#444444; display:flex; flex-direction: row; align-items: center; justify-content: space-between; border-bottom:solid 1px #cccccc; ">
	<div style="font-size:18px; color:#ffffff; font-weight:300; ">TEST WORK</div>
	<?php if(!$is_admin){ ?>
		<div onclick="document.location='index.php?q=auth'" style="border:solid 1px #eeeeee; background-color:#444444; color:#ffffff; border-radius:10px; padding:5px; padding-left:10px; padding-right:10px; text-align:center; font-size:11px; cursor:pointer; ">Авторизация</div>
	<?php }else{ ?>
		<div>
			<div style="font-size:12px; color:#ffffff; ">Вы вошли как Администратор <span onclick="document.location='index.php?q=logout';" style="color:#777777; text-decoration:underline; font-size:11px; padding:10px; cursor:pointer;">выйти</span></div>
			
		</div>
	<?php } ?>
</div>