<div style="height:70px;"></div>
<style>
	.sortButtons{
		margin-left:10px; 
		background-color:#ffffff; 
		border:solid 1px #cccccc; 
		color:#777777; 
		text-align: center; 
		padding:5px; 
		padding-left:20px; 
		padding-right:20px;
		border-radius:7px;
		cursor:pointer;
		font-size:10px;
	}

	.sortButtons.active{
		margin-left:10px; 
		border:solid 1px #ff5050;
		background-color:#ff5050 !important; 
		color:#ffffff; 
		text-align: center; 
		padding:5px; 
		padding-left:20px;
		padding-right:20px;
		border-radius:7px;
	}

	.sortButtons:hover{
		border-color:#ff5050;
	}

</style>
<div style="padding:15px; font-size:24px; padding-bottom:10px; padding-left:20px; margin-bottom:0px; margin-top:-20px;">Задачи</div>
<div style="display:flex; align-items:center; justify-content: flex-start; margin-bottom:20px; margin-left:20px;">
	<div style="font-size:10px; color:#777777;">Сортировать по:</div>
	<div onclick="change_sort('id'); $('.sortButtons').removeClass('active'); $(this).addClass('active');" class="sortButtons <?php if($sort_name == "id"){ ?>active<?php } ?>">Дате добавления</div>
	<div onclick="change_sort('name'); $('.sortButtons').removeClass('active'); $(this).addClass('active');" class="sortButtons <?php if($sort_name == "name"){ ?>active<?php } ?>">Имя пользователя</div>
	<div onclick="change_sort('email'); $('.sortButtons').removeClass('active'); $(this).addClass('active');" class="sortButtons <?php if($sort_name == "email"){ ?>active<?php } ?>">Email</div>
	<div onclick="change_sort('status'); $('.sortButtons').removeClass('active'); $(this).addClass('active');" class="sortButtons <?php if($sort_name == "status"){ ?>active<?php } ?>">Статусу</div>
</div>

<div class="container-fluid" style="padding-left:20px;">
	<div class="row" id="TasksCOntainer">