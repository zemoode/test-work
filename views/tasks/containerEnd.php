</div>
</div>

<div style="padding:15px; font-size:15px; font-weight: bold; padding-bottom:30px; width:300px; display:flex; align-items: center; justify-content: center; margin:0px auto; margin-top:20px;">
	<?php for($i = 0; $i<(($count_tasks_items/3)); $i++){ ?>
			<div onclick="get_tasks(<?=$i?>); $('.paginationButton').removeClass('active'); $(this).addClass('active');" class="paginationButton <?php if($i==0){ echo 'active'; } ?>"><?=($i+1)?></div>
	<?php } ?>
	

</div>

<div style="padding:40px;  margin-top:30px; padding-bottom:10px; padding-top:20px; background-color:#dddddd;  border-bottom:solid 1px #cccccc;">
	<div style="padding:15px; font-size:24px; padding-bottom:30px; padding-left:0px;">Добавить задачу</div>
	<div style="width:100%;  display:flex; align-items: flex-start; justify-content: space-between; padding-bottom:40px;">
		<div style="width:30%;">
			<div class="input-label">
				Имя
			</div>
			<div style="margin-top:3px;">
				<div style="padding:5px; display:none; padding-left:0px; font-size:12px; font-weight:bold; color:#ff5050;" id="post_name_error">Неодпустимое значение поля Имя ( > 3 символов ).</div>
				<input id="post_name" type="text" class="input-text" />
			</div>
		

			<div class="input-label">
				Email
			</div>
			<div style="margin-top:3px;">
				<div style="padding:5px; display:none; padding-left:0px; font-size:12px; font-weight:bold; color:#ff5050;" id="post_email_error">Неодпустимое значение поля Email <br/>( формат: ...@... && > 3 символов ).</div>
				<input id="post_email" type="text" class="input-text" />
			</div>
		</div>

		<div style="width:30%;">
			<div class="input-label">
				Текст задачи
			</div>
			<div style="margin-top:3px;">
				<div style="padding:5px; display:none; padding-left:0px; font-size:12px; font-weight:bold; color:#ff5050;" id="post_text_error">Неодпустимое значение поля "Текст задачи" ( > 3 символов ).</div>
				<textarea id="post_text" class="textarea-text" ></textarea>
			</div>
		

			
		</div>

		<div style="width:30%;">
			<div onclick="add_task($('#post_text').val(), $('#post_email').val(), $('#post_name').val());" class="forms-button">Добавить</div>
		</div>

		
	</div>

	</div>
</div>