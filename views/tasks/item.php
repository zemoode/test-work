<div class="col-4" style="position:relative;">
		
		<div style="position:absolute; right:15px; top:-25px;">
			<?php if($is_admin){ ?>
				<?php if($item['status'] == 0){ ?>
				<div class="statusButton active" onclick="change_task_values(<?=$item['id']?>, 'status', 1); $('#itemStatusActive<?=$item['id']?>').fadeIn(500); $(this).removeClass('active'); $(this).html('Отметить как не проверенное'); $(this).on('click', function(){ change_task_values(<?=$item['id']?>, 'status', 0); });" >
					Отметить как проверенное
				</div>
				<?php }else{
					?>
					<div class="statusButton" onclick="change_task_values(<?=$item['id']?>, 'status', 0); $('#itemStatusActive<?=$item['id']?>').fadeOut(500); $(this).addClass('active'); fadeOut(500); $(this).html('Отметить как проверенное'); $(this).on('click', function(){ change_task_values(<?=$item['id']?>, 'status', 1); });" >
						Отметить как не проверенное
									</div>
					<?php
				} ?>
			<?php } ?>


		</div>
		<div style="padding:15px; border:solid 1px #cccccc; width:calc(100%); background-color:#f3f3f3; overflow:hidden; box-shadow: 5px 5px 15px rgba(0,0,0,0.2);">
			<div <?php if($is_admin){ ?>contenteditable="true" pid="<?=$item['id']?>" dataname="name"<?php } ?> style="font-size:17px; font-weight:700;"><?=$item['name']?></div>
			<div <?php if($is_admin){ ?>contenteditable="true"  pid="<?=$item['id']?>"dataname="email"<?php } ?> style="font-size:14px; font-weight:400; margin-top:0px;"><?=$item['email']?></div>
			<div  style="font-size:12px; font-weight:700; margin-top:3px;"><?=date('d.m.Y H:i:s', strtotime($item['atDate']));?></div>
			
			<div id="itemStatusActive<?=$item['id']?>" style="<?php if($item['status'] == 0){ ?>display:none;<?php } ?> font-size:14px; margin-top:5px; color:#ff5050; margin-bottom:5px; font-style:italic;">(проверенно)</div>
			
			<div <?php if($is_admin){ ?>contenteditable="true" pid="<?=$item['id']?>" dataname="post"<?php } ?> style="font-size:17px; font-weight:300; margin-top:10px; white-space: wrap;"><?=stripslashes($item['post'])?></div>
		</div>
	
</div>